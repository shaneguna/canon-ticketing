<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Canon Ticketing System') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


    <script src="{{ asset('js/modernizr/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
</head>
<body id="minovate" class="appWrapper sidebar-xs-forced sidebar-xs">
<div id="app">
    {{--<div id="wrap" class="animsition">--}}
        @include('partials.header')
        <section id="content">
            <div class="page page-dashboard">
                @yield('content')
            </div>
        </section>
    {{--</div>--}}
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/all.js') }}"></script>
</body>
</html>
