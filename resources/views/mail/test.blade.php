<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Canon Checkout Support</title>
    <meta name="viewport" content="width=device-width"/>
    <style type="text/css">
        @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
            body[yahoo] .buttonwrapper {
                background-color: transparent !important;
            }

            body[yahoo] .button {
                padding: 0 !important;
            }

            body[yahoo] .button a {
                background-color: #e74c3c;
                padding: 15px 25px !important;
            }
        }

        @media only screen and (min-device-width: 601px) {
            .content {
                width: 600px !important;
            }

            .col387 {
                width: 387px !important;
            }
        }
    </style>
</head>
<body bgcolor="#EAEAEA" style="margin: 0; padding: 0;" yahoo="fix">
<!--[if (gte mso 9)|(IE)]>
<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>
<![endif]-->
<table align="center" border="0" cellpadding="0" cellspacing="0"
       style="border-collapse: collapse; width: 100%; max-width: 600px;" class="content">
    <tr>
        <td align="center" bgcolor="#CE2127"
            style="padding: 20px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 36px; font-weight: bold;">
            <img src="{{asset('images/checkout-logo.png')}}" style="height: 120px;">
        </td>
    </tr>
    <tr>
        <td align="center" bgcolor="#ffffff"
            style="padding: 25px 20px 25px 20px; color: #555555; font-family: Arial, sans-serif; font-size: 14px; line-height: 30px; border-bottom: 1px solid #f6f6f6;">
            <b style="font-size: 20px;">Thank you for contacting our support team.</b><br/>
            A support ticket has been opened for you. Our team will provide a feedback through email as soon as
            possible.
            <p style="padding-top: 20px;">The details of your ticket are shown below.</p>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" style="padding: 10px 20px 0 45px; border-bottom: 1px solid #f6f6f6;">
            <table width="700" align="left" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 0 20px 20px 0; font-family: Arial, sans-serif; font-size: 14px;">
                        <span style="text-transform:uppercase;"><b>Contact Information:</b></span>
                        <table>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Name:</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name}}
                                </td>
                            </tr>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Email:</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name }}
                                </td>
                            </tr>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Contact No.:</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name }}
                                </td>
                            </tr>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Contact No. (alternate):</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name }}
                                </td>
                            </tr>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Store:</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name }}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#ffffff" style="padding: 20px 20px 0 45px; border-bottom: 1px solid #f6f6f6;">
            <table width="700" align="left" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding: 0 20px 20px 0; font-family: Arial, sans-serif; font-size: 14px;">
                        <span style="text-transform:uppercase;"><b>Concern Details:</b></span>
                        <table>
                            <tr>
                                <td width="150" width="110" style="line-height: 24px; vertical-align:top;">
                                    <span>Ticket No.:</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name }}
                                </td>
                            </tr>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Ticket Type:</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name }}
                                </td>
                            </tr>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Order No.:</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name }}
                                </td>
                            </tr>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Subject:</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name }}
                                </td>
                            </tr>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Description:</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    {{ $name }}
                                </td>
                            </tr>
                            <tr>
                                <td width="150" style="line-height: 24px; vertical-align:top;">
                                    <span>Attachments(s):</span>
                                </td>
                                <td width="500" style="line-height: 24px; padding-left: 20px;">
                                    @if($name)
                                        <img src="{{asset('images/checkout-logo.png')}}"
                                             style="height: 320px; width:auto;">
                                    @else
                                        No attached file(s).
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 15px 10px 15px 10px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center" width="100%"
                        style="color: #999999; font-family: Arial, sans-serif; font-size: 14px;">
                        2017 &copy; <a href="http://www.itp-sven.ph/" style="color: #e74c3c;">ITP-SVEN</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!--[if (gte mso 9)|(IE)]>
</td>
</tr>
</table>
<![endif]-->
</body>
</html>