@extends('layouts.app')

@section('title', 'Open Ticket')

@section('content')
    <div class="row">
        <section class="tile">

            <div class="tile-header dvd dvd-btm"><h3 class="custom-font text-uppercase">How can we help you?</h3></div>

            <div class="tile-body">

                <create-ticket-form ></create-ticket-form>
                {{--@include('includes.flash')--}}

                {{--<form class="form-horizontal" role="form" method="POST" action="{{ url('/new_ticket') }}"--}}
                      {{--enctype="multipart/form-data">--}}
                    {{--{!! csrf_field() !!}--}}

                    {{--<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">--}}
                        {{--<label for="title" class="col-sm-2 control-label">Subject</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<input id="subject" type="text" class="form-control" name="subject"--}}
                                   {{--value="{{ old('subject') }}">--}}

                            {{--@if ($errors->has('subject'))--}}
                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('subject') }}</strong>--}}
                                    {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
                        {{--<label for="title" class="col-sm-2 control-label">Name</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<input id="name" type="text" class="form-control" name="name"--}}
                                   {{--value="{{ old('name') }}">--}}

                            {{--@if ($errors->has('name'))--}}
                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('name') }}</strong>--}}
                                    {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                        {{--<label for="title" class="col-sm-2 control-label">Email</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<input id="email" type="email" class="form-control" name="email"--}}
                                   {{--value="{{ old('email') }}">--}}

                            {{--@if ($errors->has('email'))--}}
                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }}">--}}
                        {{--<label for="title" class="col-sm-2 control-label">Contact No.</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<input id="contact_no" type="text" class="form-control" name="contact_no"--}}
                                   {{--value="{{ old('contact_no') }}">--}}

                            {{--@if ($errors->has('contact_no'))--}}
                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('contact_no') }}</strong>--}}
                                    {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group{{ $errors->has('alt_contact_no') ? ' has-error' : '' }}">--}}
                        {{--<label for="title" class="col-sm-2 control-label">Contact No. (Optional)</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<input id="alt_contact_no" type="text" class="form-control" name="alt_contact_no"--}}
                                   {{--value="{{ old('alt_contact_no') }}">--}}

                            {{--@if ($errors->has('alt_contact_no'))--}}
                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('alt_contact_no') }}</strong>--}}
                                    {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group{{ $errors->has('store') ? ' has-error' : '' }}">--}}
                        {{--<label for="store" class="col-sm-2 control-label">Store</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<select id="store" type="store" class="form-control" name="store">--}}
                                {{--<option value="">Select Category</option>--}}
                                {{--@foreach ($stores as $store)--}}
                                    {{--<option value="{{ $store->id }}">{{ $store->name }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}

                            {{--@if ($errors->has('store'))--}}
                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('store') }}</strong>--}}
                                    {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group{{ $errors->has('ticket_type') ? ' has-error' : '' }}">--}}
                        {{--<label for="ticket_type" class="col-sm-2 control-label">Ticket Type</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<select id="ticket_type" type="ticket_type" class="form-control" name="ticket_type">--}}
                                {{--<option value="">Select Category</option>--}}
                                {{--@foreach ($ticket_type as $ticket)--}}
                                    {{--<option value="{{ $ticket->id }}">{{ $ticket->name }}</option>--}}
                                {{--@endforeach--}}
                            {{--</select>--}}

                            {{--@if ($errors->has('ticket_type'))--}}
                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('ticket_type') }}</strong>--}}
                                    {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group{{ $errors->has('order_no') ? ' has-error' : '' }}">--}}
                        {{--<label for="title" class="col-sm-2 control-label">Order No. (Optional)</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<input id="order_no" type="text" class="form-control" name="order_no"--}}
                                   {{--value="{{ old('order_no') }}">--}}

                            {{--@if ($errors->has('order_no'))--}}
                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('order_no') }}</strong>--}}
                                    {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group{{ $errors->has('attached_file') ? ' has-error' : '' }}">--}}
                        {{--<label class="col-sm-2 control-label">File Input</label>--}}
                        {{--<div class="col-sm-10">--}}
                            {{--<input type="file" name="attached_file" class="filestyle" data-buttonText="Find file" data-iconName="fa fa-inbox">--}}
                        {{--</div>--}}
                        {{--@if ($errors->has('attached_file'))--}}
                            {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('attached_file') }}</strong>--}}
                                    {{--</span>--}}
                        {{--@endif--}}
                    {{--</div>--}}


                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group{{ $errors->has('Description') ? ' has-error' : '' }}">--}}
                        {{--<label for="Description" class="col-sm-2 control-label">Description</label>--}}

                        {{--<div class="col-sm-10">--}}
                            {{--<textarea rows="10" id="Description" class="form-control" name="description"></textarea>--}}

                            {{--@if ($errors->has('Description'))--}}
                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('Description') }}</strong>--}}
                                    {{--</span>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<hr class="line-dashed line-full"/>--}}

                    {{--<div class="form-group">--}}
                        {{--<div class="col-sm-2 col-sm-offset-2">--}}
                            {{--<button type="submit" class="btn btn-primary">--}}
                                {{--<i class="fa fa-btn fa-ticket"></i> Submit--}}
                            {{--</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</form>--}}
            </div>

        </section>
    </div>
@endsection