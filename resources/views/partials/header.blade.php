<!-- ===============================================
            ================= HEADER Content ===================
            ================================================ -->
<section id="header">
    <header class="clearfix">

        <!-- Branding -->
        <div class="branding">
            <a><img style="padding: 6px;" src="{{asset('/images/checkout-logo25x25.png')}}"></a>
            <a class="brand" href="{{url('/')}}">
                <span class="text-uppercase">Canon Checkout Support</span>
            </a>
            <a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline"><i class="fa fa-bars"></i></a>
        </div>
        <!-- Branding end -->


    @include('partials.left-side-nav')
    @include('partials.sidebar')


    <!-- Search -->
    {{--<div class="search" id="main-search">--}}
    {{--<input type="text" class="form-control underline-input" placeholder="Search...">--}}
    {{--</div>--}}
    <!-- Search end -->

    </header>

</section>
<!--/ HEADER Content  -->