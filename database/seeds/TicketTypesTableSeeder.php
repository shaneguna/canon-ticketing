<?php

use Illuminate\Database\Seeder;

class TicketTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ticket_types')->delete();
        
        \DB::table('ticket_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Incident',
                'created_at' => '2017-04-27 00:00:00',
                'updated_at' => '2017-04-27 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Inquiry',
                'created_at' => '2017-04-27 00:00:00',
                'updated_at' => '2017-04-27 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Problem',
                'created_at' => '2017-04-27 00:00:00',
                'updated_at' => '2017-04-27 00:00:00',
            ),
        ));
        
        
    }
}