<?php

namespace App\Http\Controllers\Api;

use App\TicketType;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Ticket;
use App\Http\Traits\APITrait;
use Illuminate\Support\Facades\Cache;
use App\Transformers\TicketTypeTransformer;
use App\Transformers\StoreTransformer;
use App\Mail\TicketCreated;
use App\Mail\AgentMailer;

class TicketsController extends ApiController
{
    use APITrait;
    protected $bcc = ['shanechioguna@gmail.com','pyu@itp-sven.ph','klua@itp-sven.ph'];

    public function store(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',
            'name' => 'required',
            'email' => 'required',
            'contact_no' => 'required',
            'selected_store' => 'required',
            'selected_ticket_type' => 'required',
            'description' => 'required',
        ]);

        $ticket = new Ticket([
            'ticket_id' => strtoupper( rand(10,50).str_random(4)),
            'subject' => $request->input('subject'),
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'contact_no' => $request->input('contact_no'),
            'alternate_contact_no' => $request->input('alt_contact_no'),
            'store' => $request->input('selected_store'),
            'order_no' => $request->input('order_no'),
            'attached_file' => $request->input('image'),
            'description' => $request->input('description'),
            'ticket_type' => $request->input('selected_ticket_type')
        ]);

        $ticket->save();

        $mailData = $ticket;

        \Mail::to($request->input('email'))
            ->bcc($this->bcc)
            ->send(new TicketCreated($mailData));
        \Mail::to($request->input('email'))
            ->bcc($this->bcc)
            ->send(new AgentMailer($mailData));


        $message = 'A ticket with ID: #'.$mailData->ticket_id.' has been opened.';
        $data = compact('message');

        return $this->respondWithArray($data);

        //return redirect()->back()->with("status", "A ticket with ID: #$ticket->id has been opened.");
    }

    public function getStoreList() {

        $this->attemptLogin();

        $client = new Client; //GuzzleHttp\Client

        if (Cache::has('jwt-token')) {

            foreach (Cache::get('jwt-token') as $value) {
                $jwt_token = $value;
            }

            try {
                $result = $client
                    ->post($this->getWebURI() . 'api/v1/get-store-list',
                        [
                            'headers' => [
                                'Authorization' => 'Bearer ' . $jwt_token
                            ]
                        ]
                    )->getBody();
                $obj = json_decode($result);
                $stores = $obj->data;
            } catch (RequestException $e) {
                $exception = (string)$e->getResponse()->getBody();
                $exception = json_decode($exception);
                // unset jwt-token at error exception

                return response()->view('errors.' . $e->getCode(), [$exception], $e->getCode());
            }
            return $this->respondWithCollection($stores, new StoreTransformer);
        }
    }

    public function getTicketTypeList()
    {
        $ticket_type = TicketType::all();

        return $this->respondWithCollection($ticket_type, new TicketTypeTransformer);
    }
}
