<?php

namespace App\Http\Controllers;

use App\Mail\TicketCreated;
use App\Mail\AgentMailer;
use Illuminate\Http\Request;
use App\TicketType;
use App\Ticket;
use Illuminate\Support\Facades\Storage;

class TicketsController extends Controller
{

    protected $bcc = ['shanechioguna@gmail.com','pyu@itp-sven.ph','klua@itp-sven.ph'];

    public function create()
    {
        return view('tickets.create', compact('ticket_type', 'stores'));
    }
}
