<?php

namespace App\Http\Traits;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

trait APITrait
{
    /**
     * @return string
     */
    public function getWebURI()
    {
        return 'http://52.77.124.42/';
        //return 'http://testbed.tokyo/checkout_admin/';
        //return 'http://ec2-35-160-15-171.us-west-2.compute.amazonaws.com/';
        //return config('kiosk.api_uri');
        //return 'http://canonadmin.dev:8081/';
    }

    public function attemptLogin()
    {
        $expiresAt = Carbon::now()->addMinutes(5);

        $client = new Client; //GuzzleHttp\Client

        try {
            $result = $client
                ->post($this->getWebURI() . 'api/v1/attempt-sign-in',
                    [
                        'form_params' => [
                            'email' => 'mobileapp@gmail.com',
                            'password' => '123456'
                        ]
                    ]
                )
                ->getBody();

            $obj = json_decode($result);

            Cache::put('jwt-token', $obj->token , $expiresAt);

        } catch (RequestException $e) {
            $exception = (string)$e->getResponse()->getBody();
            $exception = json_decode($exception);
            // unset jwt-token at error exception

            return response()->view('errors.' . $e->getCode(), [$exception], $e->getCode());
        }
    }
}