<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketCreated extends Mailable
{
    use Queueable, SerializesModels;
    protected $mailData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $input = array(
            'ticket' => $this->mailData
        );

        $subject = 'Report Received (#' . $this->mailData->ticket_id . '):  ' . $this->mailData->subject;
        if (count($this->mailData->attached_file) > 0) {
            return $this->view('mail.ticket_created')
                ->subject($subject)
                ->attach($this->mailData->attached_file)
                ->with('ticket', $input);
        } else {
            return $this->view('mail.ticket_created')
                ->subject($subject)
                ->with('ticket', $input);
        }
    }
}
