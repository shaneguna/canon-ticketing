<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'subject',
        'ticket_id',
        'name',
        'email',
        'contact_no',
        'alternate_contact_no',
        'store',
        'ticket_type',
        'order_no',
        'attached_file',
        'description'
    ];

    public function ticketType()
    {
        return $this->belongsTo(TicketType::class);
    }
}
