<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class StoreTransformer extends TransformerAbstract
{
    public function transform($stores)
    {
        return [
            'id'            => $stores->id,
            'name'          => $stores->name
        ];
    }
}
