<?php

namespace App\Transformers;

use App\TicketType;
use League\Fractal\TransformerAbstract;

class TicketTypeTransformer extends TransformerAbstract
{
    public function transform(TicketType $ticket_type)
    {
        return [
            'id'            => $ticket_type->id,
            'name'          => $ticket_type->name
        ];
    }
}
