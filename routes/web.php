<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'TicketsController@create');

Route::get('/test', function () {
    $src = ['9' => collect(['x' => 'xzy'],'a'), '8' => collect(['x' => 'xzy']), '7' => collect(['x' => 'xzy'])];

    foreach ($src as $index => $value) {
            $var[$index] = $value;
    }
    dd($var);

    //return view('mail.test', compact('name'));
});

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('/kick', function(){
    Auth::logout();
});
