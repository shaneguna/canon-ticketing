const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .styles(['resources/assets/minovate-assets/css/vendor/bootstrap.min.css',
        'resources/assets/minovate-assets/css/vendor/animate.css',
        'resources/assets/minovate-assets/css/vendor/animate.css',
        'resources/assets/minovate-assets/css/vendor/font-awesome.min.css',
        'resources/assets/minovate-assets/js/vendor/animsition/css/animsition.min.css',
        'resources/assets/minovate-assets/js/vendor/daterangepicker/daterangepicker-bs3.css',
        'resources/assets/minovate-assets/js/vendor/morris/morris.css',
        'resources/assets/minovate-assets/js/vendor/owl-carousel/owl.carousel.css',
        'resources/assets/minovate-assets/js/vendor/owl-carousel/owl.theme.css',
        'resources/assets/minovate-assets/js/vendor/rickshaw/rickshaw.min.css',
        'resources/assets/minovate-assets/js/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css',
        'resources/assets/minovate-assets/js/vendor/datatables/css/jquery.dataTables.min.css',
        'resources/assets/minovate-assets/js/vendor/datatables/datatables.bootstrap.min.css',
        'resources/assets/minovate-assets/js/vendor/chosen/chosen.css',
        'resources/assets/minovate-assets/js/vendor/summernote/summernote.css',
        'resources/assets/minovate-assets/css/main.css',
        'node_modules/vue-multiselect/dist/vue-multiselect.min.css'], 'public/css/all.css')
    .scripts(['resources/assets/minovate-assets/js/vendor/jRespond/jRespond.min.js',
        'resources/assets/minovate-assets/js/vendor/d3/d3.min.js',
        'resources/assets/minovate-assets/js/vendor/d3/d3.layout.min.js',
        'resources/assets/minovate-assets/js/vendor/rickshaw/rickshaw.min.js',
        'resources/assets/minovate-assets/js/vendor/sparkline/jquery.sparkline.min.js',
        'resources/assets/minovate-assets/js/vendor/slimscroll/jquery.slimscroll.min.js',
        'resources/assets/minovate-assets/js/vendor/animsition/js/jquery.animsition.min.js',
        'resources/assets/minovate-assets/js/vendor/daterangepicker/moment.min.js',
        'resources/assets/minovate-assets/js/vendor/daterangepicker/daterangepicker.js',
        'resources/assets/minovate-assets/js/vendor/screenfull/screenfull.min.js',
        'resources/assets/minovate-assets/js/vendor/flot/jquery.flot.min.js',
        'resources/assets/minovate-assets/js/vendor/flot-tooltip/jquery.flot.tooltip.min.js',
        'resources/assets/minovate-assets/js/vendor/flot-spline/jquery.flot.spline.min.js',
        'resources/assets/minovate-assets/js/vendor/easypiechart/jquery.easypiechart.min.js',
        'resources/assets/minovate-assets/js/vendor/raphael/raphael-min.js',
        'resources/assets/minovate-assets/js/vendor/morris/morris.min.js',
        'resources/assets/minovate-assets/js/vendor/owl-carousel/owl.carousel.min.js',
        'resources/assets/minovate-assets/js/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js',
        'resources/assets/minovate-assets/js/vendor/datatables/js/jquery.dataTables.min.js',
        'resources/assets/minovate-assets/js/vendor/datatables/extensions/dataTables.bootstrap.js',
        'resources/assets/minovate-assets/js/vendor/chosen/chosen.jquery.min.js',
        'resources/assets/minovate-assets/js/vendor/summernote/summernote.min.js',
        'resources/assets/minovate-assets/js/vendor/coolclock/coolclock.js',
        'resources/assets/minovate-assets/js/vendor/coolclock/excanvas.js',
        'resources/assets/minovate-assets/js/main.js'], 'public/js/all.js');
